#ifndef Tasks_H_

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "Histogram.h"
#include "Segmentation.h"
#include "Reflection.h"
#include "GaussianFilter.h"
#include "xAxisReflection.h"
#include "Task2.h"
#include "Sonal.h"
#include "ScaledImage.h"
#include "Shubham.h"

using namespace std;

vector<int> getInput(string s);

void IMT2016001(char* filename);
void IMT2016029(char* filename);
void IMT2016057(char* filename);
void IMT2016079(char* filename);
void IMT2016099(char* filename);
void IMT2016118(char* filename);

#endif
