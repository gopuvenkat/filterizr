C++ Project

Group Number : 5

Members:
  1. IMT2016001 - Gopalakrishnan V
  2. IMT2016029 - Srikar
  3. IMT2016057 - Manonmaie
  4. IMT2016079 - Sonal Garg
  5. IMT2016099 - Raghavan GV
  6. IMT2016118 - Shubham Gupta

Individual Contributions:

  Gopalakrishnan.V - IMT2016001
    1. Wrote the base classes
    2. Implemented task 4
    3. Integrated others work into a single project

  Srikar - IMT2016029
    1. Implemented task 7

  Manonmaie - IMT2016057
    1. Contributed in integrating others work
    2. Completed task 3

  Sonal Garg - IMT2016079
    1. Completed task 5

  Raghavan GV - IMT2016099
    1. Completed task 2

  Shubham Gupta - IMT2016118
    1. Completed task 6


To run

g++ *.cpp
./a.out

Note : Works with .ppm files

